<!DOCTYPE html>
<html lang="en">
<head>

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<link href="https://assets.openport.com/images/favicon-openport-32x32.png" rel="shortcut icon" type="image/png" />

<meta property="og:title"		content="OpenPort Driver App"/>
<meta property="og:type" 		content="website"/>
<meta property="og:image" 		content="//assets.openport.com/images/fbog-openport-dark.png"/>
<meta property="og:url" 		content="//openport.com/app"/>
<!--<meta property="og:description" content="5.11.0-b285"/>-->

<title>OpenPort Driver App</title>

<!--<link href="https://openport.com/wp-content/themes/grayscale/style.css?ver=4.9.2" rel="stylesheet">-->
<link href="https://assets.openport.com/opfw/gswp-mod_1805.css?20180524" rel="stylesheet">
<link href="https://assets.openport.com/webfonts/type.css?20180524" rel="stylesheet">

<style>

html { font-family: Real Text }
.button--fill { font-family: Hurme Geometric Sans }

.container { text-align: center; max-width: 320px; margin: 0 auto;   
display: flex; min-height: 100vh;
flex-direction: column; 
}
.container > * {
display:flex;
flex-direction: column; 

}
img { max-width: 75%; margin: 2rem auto 1rem !important }

.version { font-size: 11px; }

.app-footer {
flex:1;
background: #fff  url(openport-app-mockup.jpg) no-repeat;
background-size: 90% auto;
background-position: center 1rem;
}

</style>

</head>
<body>

	<div class="container">
	
		<header>
			<p><img src="https://assets.openport.com/images/logo-openport-dark.svg"></p>
		</header>

		<main>
			<p><a class="button--fill" href="//assets.openport.com/APK/driver-prd-v5.15.0-b369-20181122.apk">Download APK</a></p>
			<p class="version">v5.15.0-b369</p>
		</main>
		
		
<?php
# Most recently updated file in a directory

# Set up

$dir = "/var/www/assets.openport.com/html/APK";
$pattern = '\.(html|php|php4)$';

$newstamp = 0;
$newname = "";
$dc = opendir($dir);
while ($fn = readdir($dc)) {
        # Eliminate current directory, parent directory
        if (ereg('^\.{1,2}$',$fn)) continue;
        # Eliminate other pages not in pattern
        if (! ereg($pattern,$fn)) continue;
        $timedat = filemtime("$dir/$fn");
        if ($timedat > $newstamp) {
                $newstamp = $timedat;
                $newname = $fn;
                }
        }

# $newstamp is the time for the latest file
# $newname is the name of the latest file

?>

 <a href=http://www.wellho.net/demo/<?= $newname
?>><?= $newname ?></a>
which was modified last <?= date("l, j F Y",$newstamp) ?>

	</div>

</body>
</html>